local _M = {}

function _M.table_has_value (tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end
    return false
end

function _M.table_has_key (tab, key)
    for k, value in pairs(tab) do
        if k == key then
            return true
        end
    end
    return false
end

function _M.tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function _M.dump(o)
  if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
        if type(k) ~= 'number' then k = '"'..k..'"' end
        s = s .. '['..k..'] = ' .. _M.dump(v) .. ','
      end
      return s .. '} '
  else
      return tostring(o)
  end
  return ''
end

function _M.get_table(data)
  local result = {}
  if type(data) == 'table' then 
    result = data
  else
    table.insert(result, data)
  end
  return result
end

function _M.do_request(rest_api_secret, endpoint, sub, useremail)
    local myheaders = {}
    local mydata = {
      email = useremail
    }
    myheaders['Authorization'] = rest_api_secret
    myheaders['Content-Type'] = "application/json"
    local args = { 
      headers = myheaders,
      data = mydata,
      }
    local response = require('requests').request("POST", endpoint .. '/authorize/' .. sub, args)
    return response
end

function _M.is_authorized_by_claims(claims_required, res)
  local _authorized_by_claim = false
  for claim_name, claim_value in pairs(claims_required) do
    -- ngx.say("CLAIM: " .. claim_name)
    -- ngx.say("CLAIM VALUE: " .. claim_value)
    local claim_value_table = _M.get_table(claim_value) 
    local aux = {}
    -- ngx.say(dump(res.user))
    if _M.table_has_key(res.user , claim_name) then
      aux = _M.get_table(res.user[claim_name])
      for k, cval in pairs(aux) do 
        for k2, cv in pairs(claim_value_table) do
          if cval == cv then
            -- ngx.say("\tMATCH VALUE: " .. cval)
            _authorized_by_claim = true
          end
        end  
      end
    end
  end 
  return _authorized_by_claim
end

return _M
