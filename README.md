# primage/oidc-proxy [![Image Layers](https://images.microbadger.com/badges/image/primage/oidc-proxy.svg)](https://microbadger.com/#/images/primage/oidc-proxy)

This repository is a fork modified from https://github.com/evry/docker-oidc-proxy to use in combination with [kube-authorizer](https://gitlab.com/primageproject/kube-authorizer).

Docker Image for OpenID Connect proxy authentication. Useful for putting services (Kubernetes) behind Keycloak and other OpenID Connect authentication (EGI Check-In).

This is Image used Nginx for proxying request and OpenResty with the
`lua-resty-openidc` library to handle OpenID Connect authentication.

## Supported tags and respective Dockerfile links

* [`latest` , `v1.2.0-k8s-v1`  (*Dockerfile*)](https://github.com/primageproject/oidc-proxy/blob/master/Dockerfile)

## How to use this image

The configuration of the proxy is performed using a configuration file (lua script) that must be placed in `/usr/local/openresty/nginx/lua/config_vars.lua` and some  environmental variables. The Docker image is desinged to only support Nginx using SSL, so you will mount the certificate in `/usr/local/openresty/nginx/ssl/nginx.crt` and the certificate key `/usr/local/openresty/nginx/ssl/nginx.key`.

It is required to define the following environmental variables: 
* `OID_SESSION_NAME`: name of de session (optional). Default: "oidc_auth". 
* `OID_SESSION_SECRET`: random string (required).
* `PROXY_PROTOCOL`: "http" or "https". Protocol of the service behind the Nginx.
* `PROXY_PORT`: port of the service to proxy.
* `PROXY_HOST`: host name of the service to proxy.


The configuration file has two dictionaries: `opts` and `authorizer`.

`opts` contains the configuration parameters for the NGINX proxy: 
* `ssl_verify`: check SSI or not (`on` or `off`)
* `redirect_uri_path`: Redirect path after authentication
* `discovery`: OpenID provider well-known discovery URL
* `client_id`: OpenID Client ID
* `client_secret`: OpenID Client Secret
* `scope`: OpenID Connect authentication scope
* `refresh_session_interval`: Time (in seconds) to renew of access token
* `redirect_uri_scheme`: Protocol to the service to proxy (`http` or `https`)

`authorizer` contains the parameters to connect with [kube-authorizer](https://gitlab.com/primageproject/kube-authorizer) and to restrict the access to the service behind the proxy by checking the OIDC claims.

* `kube_authorizer_params.endopoint`: kube-authorizer URI 
* `kube_authorizer_params.rest_api_secret`: kube-authorizer token for authentication
* `claims_required`: dictionary to restrict the access to the service by checking if the claims of the ID token containers each element of this dictionary. The key of each element correspond with the claim name to be checked

The following code that is available [here](https://gitlab.com/primageproject/oidc-proxy/-/blob/master/nginx/lua/config_vars.lua.template) is an example of the configuration file.
```lua
local _M = {}
_M.opts = {
    redirect_uri_path = "/redirect_uri",
    discovery = "https://XXXX/.well-known/openid-configuration",                 
    client_id = "XXXXXXXXXXXXXXXXXXXXX",
    client_secret = "XXXXXXXXXXXXXXXXXXXXXXXX",
    ssl_verify = "no",
    scope = "openid email profile XXXXXXXXXX",
    redirect_uri_scheme = "https",
    refresh_session_interval = 300, 
}

_M.authorizer = {
    kube_authorizer_params = {
        endpoint = "XXXXXXXXXXXXXXXXXXX",
        rest_api_secret = "XXXXXXXXXXXXXXX",
    },
    claims_required = {
        CLAIM_NAME = "CLAIM_VALUE"
    },
}
return _M
```


You can use the [docker image](https://hub.docker.com/r/primage/oidc-proxy) running the following code. 
```bash
docker run \
  -e PROXY_HOST=myservice.com \
  -e PROXY_PORT=8080 \
  -e PROXY_PROTOCOL=https \
  -e OID_SESSION_NAME=oidc_auth \
  -e OID_SESSION_SECRET=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 64 ; echo '') \
  -v /path/to/config_vars.lua:/usr/local/openresty/nginx/lua/config_vars.lua \
  -v /path/to/mysite.crt:/usr/local/openresty/nginx/ssl/nginx.crt \
  -v /path/to/mysite.key:/usr/local/openresty/nginx/ssl/nginx.key \
  -p 80:80 \
  primage/oidc-proxy
```

You can see an example of using this OIDC proxy in combination with[kube-authorizer](https://gitlab.com/primageproject/kube-authorizer) to manage the access in a Kubernetes Cluster [here](https://gitlab.com/primageproject/kube-authorizer/-/tree/master#example-oidc-proxy-kube-authorizer-to-manage-the-access-to-a-kubernetes-cluster).
## License

This Docker image is licensed under the [Apache License 2.0](https://github.com/evry/docker-oidc-proxy/blob/master/LICENSE).

Software contained in this image is licensed under the following:
* Base [Github project](https://github.com/evry/docker-oidc-proxy): [MIT License](https://github.com/evry/docker-oidc-proxy/blob/master/LICENSE)
* docker-openresty: [BSD 2-clause "Simplified" License](https://github.com/openresty/docker-openresty/blob/master/COPYRIGHT)
* lua-resty-http: [BSD 2-clause "Simplified" License](https://github.com/pintsized/lua-resty-http/blob/master/LICENSE)
* lua-resty-jwt: [Apache License 2.0](https://github.com/cdbattags/lua-resty-jwt/blob/master/LICENSE.txt)
* lua-resty-openidc: [Apache License 2.0](https://github.com/pingidentity/lua-resty-openidc/blob/master/LICENSE.txt)
* lua-resty-session: [BSD 2-clause "Simplified" License](https://github.com/bungle/lua-resty-session/blob/master/LICENSE)
* lua-resty-hmac: [BSD 2-clause "Simplified" License](https://github.com/jkeys089/lua-resty-hmac/#copyright-and-license)
* lua-requests: [MIT License](https://github.com/JakobGreen/lua-requests)

## Supported Docker versions

This image is officially supported on Docker version 1.12.

Support for older versions (down to 1.0) is provided on a best-effort basis.

## User Feedback

### Documentation

* [Docker](http://docs.docker.com)
* [nginx](http://nginx.org/en/docs/)
* [OpenResty](http://openresty.org/)
* [lua-resty-openidc](https://github.com/pingidentity/lua-resty-openidc#readme)

### Issues

If you have any problems with or questions about this image, please contact us
through a [Gitlab issue](https://gitlab.com/primageproject/oidc-proxy/-/issues).

### Contributing

You are invited to contribute new features, fixes, or updates, large or small;
we are always thrilled to receive pull requests, and do our best to process them
as fast as we can.

Before you start to code, we recommend discussing your plans through a [Gitlab
issue](https://gitlab.com/primageproject/oidc-proxy/-/issues), especially for more
ambitious contributions. This gives other contributors a chance to point you in
the right direction, give you feedback on your design, and help you find out if
someone else is working on the same thing.
