FROM openresty/openresty:1.13.6.2-alpine
MAINTAINER Sergio Lopez <serlohu@upv.es>

ENV \
 SESSION_VERSION=2.22 \
 HTTP_VERSION=0.12 \
 OPENIDC_VERSION=1.6.1 \
 JWT_VERSION=0.2.0 \
 HMAC_VERSION=989f601acbe74dee71c1a48f3e140a427f2d03ae \
 REQUESTS_VERSION=1.2-1 \
 LUAROCKS_VERSION=3.3.1 \
 OID_SESSION_NAME=oidc_auth
 

RUN \
 apk update && apk upgrade && apk add build-base curl make unzip gcc libc-dev openssl openssl-dev m4 bsd-compat-headers && \
 cd /tmp && \
 curl -sSL https://github.com/bungle/lua-resty-session/archive/v${SESSION_VERSION}.tar.gz | tar xz && \
 curl -sSL https://github.com/pintsized/lua-resty-http/archive/v${HTTP_VERSION}.tar.gz | tar xz  && \
 curl -sSL https://github.com/pingidentity/lua-resty-openidc/archive/v${OPENIDC_VERSION}.tar.gz | tar xz && \
 curl -sSL https://github.com/cdbattags/lua-resty-jwt/archive/v${JWT_VERSION}.tar.gz | tar xz && \
 curl -sSL https://github.com/jkeys089/lua-resty-hmac/archive/${HMAC_VERSION}.tar.gz | tar xz && \
 #curl -sSL https://github.com/JakobGreen/lua-requests/archive/${REQUESTS_VERSION}.tar.gz | tar xz && \
 curl -sSL http://luarocks.org/releases/luarocks-${LUAROCKS_VERSION}.tar.gz | tar xz && \
 cp -r /tmp/lua-resty-session-${SESSION_VERSION}/lib/resty/* /usr/local/openresty/lualib/resty/ && \
 cp -r /tmp/lua-resty-http-${HTTP_VERSION}/lib/resty/* /usr/local/openresty/lualib/resty/ && \
 cp -r /tmp/lua-resty-openidc-${OPENIDC_VERSION}/lib/resty/* /usr/local/openresty/lualib/resty/ && \
 cp -r /tmp/lua-resty-jwt-${JWT_VERSION}/lib/resty/* /usr/local/openresty/lualib/resty/ && \
 cp -r /tmp/lua-resty-hmac-${HMAC_VERSION}/lib/resty/* /usr/local/openresty/lualib/resty/ && \
 #cp -r /tmp/lua-requests-${REQUESTS_VERSION}/src/requests.lua /usr/local/openresty && \
 cd luarocks-${LUAROCKS_VERSION} && ./configure && make build && make install && \
 rm -rf /tmp/* && \
 mkdir -p /usr/local/openresty/nginx/conf/hostsites/ && \
 true


RUN luarocks install LuaSocket && \
 luarocks install LuaSec && \ 
 luarocks install md5 && \
 luarocks install lbase64 && \ 
 luarocks install xml && \
 luarocks install cqueues && \
 luarocks install http && \
 luarocks install lua-requests 1.2-0

COPY bootstrap.sh /usr/local/openresty/bootstrap.sh
COPY nginx /usr/local/openresty/nginx/

ENTRYPOINT ["/usr/local/openresty/bootstrap.sh"]
